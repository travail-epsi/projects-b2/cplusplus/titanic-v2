#pragma once
#include <string>
#include "../Database.h"

class Penalite
{
private:
	int codePenalite;
	std::string nom;
	double duree;
	double longitude;
	double latitude;
	std::string description;

public:
	Penalite(std::string, double duree, double longitude, double latitude, std::string description);

	int get_id();
	std::string get_nom();
	std::string set_nom(std::string new_nom);
	double get_duree();
	double set_duree(double duree);
	double get_longitude();
	double set_longitude(double longitude);
	double get_latitude();
	double set_latitude(double latitude);
	std::string get_description();
	std::string set_description(std::string new_description);
	void delete_self();
};

