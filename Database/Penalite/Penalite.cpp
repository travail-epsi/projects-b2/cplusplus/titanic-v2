#include "Penalite.h"

Penalite::Penalite(std::string nom, double duree, double longitude, double latitude, std::string description) {
	if (nom == "" || !duree || !longitude || !latitude || description == "") {
		std::cout << "not all param given where set correctly" << std::endl;
		return;
	}

	Database db;
	mysql_free_result(db.request("INSERT INTO Penalite (nom, duree, longitude, latitude, description) VALUES (\"" + nom + '\"' + ", " + std::to_string(duree) + ", " + std::to_string(longitude) + ", " + std::to_string(latitude) + ", \"" + description + "\")"));
	this->nom = "";
	this->duree = duree;
	this->longitude = longitude;
	this->latitude = latitude;
	this->description = description;

	MYSQL_RES* res = db.request("SELECT LAST_INSERT_ID()");
	if (res) {
		MYSQL_ROW row;
		while ((row = mysql_fetch_row(res)) != 0) {
			// converts ascii to int
			this->codePenalite = atoi(row[0]);
		}
	}
	mysql_free_result(res);
}


int Penalite::get_id() {
	if (this->codePenalite == 0) {
		Database db;
		MYSQL_RES* res = db.request("SELECT codePenalite FROM Penalite WHERE nom = \"" + this->nom + "\" AND duree = " + std::to_string(this->duree) + " AND longitude = " + std::to_string(this->longitude) + " AND latitude = " + std::to_string(this->latitude) + " AND description = \"" + description + '\"');
		MYSQL_ROW row;
		if (res) {
			MYSQL_ROW row;
			while ((row = mysql_fetch_row(res)) != 0) {
				// converts ascii to int
				this->codePenalite = atoi(row[0]);
			}
		}
		mysql_free_result(res);
	}

	return this->codePenalite;
}

std::string Penalite::get_nom() {
	if (this->nom == "") {
		Database db;
		MYSQL_RES* res;
		if (this->codePenalite == 0) {
			res = db.request("SELECT nom FROM Penalite WHERE duree = " + std::to_string(this->duree) + " AND longitude = " + std::to_string(this->longitude) + " AND latitude = " + std::to_string(this->latitude) + " AND description = \"" + this->description + '\"');
		}
		else {
			res = db.request("SELECT nom FROM Penalite WHERE codePenalite = " + std::to_string(this->codePenalite));
		}
		MYSQL_ROW row;
		if (res) {
			MYSQL_ROW row;
			row = mysql_fetch_row(res);
			this->nom = row[0];
		}
		mysql_free_result(res);
	}
	return this->nom;
}

std::string Penalite::set_nom(std::string new_nom) {
	if (new_nom == "") {
		std::cout << "new name needs to be longer than 0" << std::endl;
		return new_nom;
	}

	Database db;
	if (this->codePenalite == 0) {
		mysql_free_result(db.request("UPDATE Penalite SET nom = \"" + new_nom + "\" WHERE duree = " + std::to_string(this->duree) + " AND longitude = " + std::to_string(this->longitude) + " AND latitude = " + std::to_string(this->latitude) + " AND description = \"" + this->description + '\"'));
	}
	else {
		mysql_free_result(db.request("UPDATE Penalite SET nom = \"" + new_nom + "\" WHERE codePenalite = " + std::to_string(this->codePenalite)));
	}

	this->nom = new_nom;
	return this->nom;
}

double Penalite::get_duree() {
	if (!this->duree) {
		Database db;
		MYSQL_RES* res;
		if (this->codePenalite == 0) {
			res = db.request("SELECT duree FROM Penalite WHERE nom = \"" + this->nom + "\" AND longitude = " + std::to_string(this->longitude) + " AND latitude = " + std::to_string(this->latitude) + " AND description = \"" + this->description + '\"');
		}
		else {
			res = db.request("SELECT duree FROM Penalite WHERE codePenalite = " + std::to_string(this->codePenalite));
		}
		MYSQL_ROW row;
		if (res) {
			MYSQL_ROW row;
			while ((row = mysql_fetch_row(res)) != 0) {
				// converts ascii to int
				this->duree = atoi(row[0]);
			}
		}
		mysql_free_result(res);
	}
	return this->duree;
}

double Penalite::set_duree(double new_duree) 
{
	if (!new_duree) {
		std::cout << "new duration needs to be longer" << std::endl;
		return new_duree;
	}

	Database db;
	MYSQL_RES* res;
	if (this->codePenalite == 0) {
		mysql_free_result(db.request("UPDATE Penalite SET duree = " + std::to_string(new_duree) + " WHERE nom = \"" + this->nom + "\" AND longitude = " + std::to_string(this->longitude) + " AND latitude = " + std::to_string(this->latitude) + " AND description = \"" + this->description + '\"'));
	}
	else {
		mysql_free_result(db.request("UPDATE Penalite SET duree = " + std::to_string(new_duree) + " WHERE codePenalite = " + std::to_string(codePenalite)));
	}

	this->duree = new_duree;
	return this->duree;
}


double Penalite::get_longitude()
{
	if (!this->latitude) {
		Database db;
		MYSQL_RES* res;
		if (this->codePenalite == 0) {
			res = db.request("SELECT longitude FROM Penalite WHERE nom = \"" + this->nom + "\" AND duree = " + std::to_string(this->duree) + " AND latitude = " + std::to_string(this->latitude) + " AND description = \"" + this->description + '\"');
		}
		else {
			res = db.request("SELECT longitude FROM Penalite WHERE codePenalite = " + std::to_string(this->codePenalite));
		}
		MYSQL_ROW row;
		if (res) {
			MYSQL_ROW row;
			while ((row = mysql_fetch_row(res)) != 0) {
				// converts ascii to int
				this->longitude = atoi(row[0]);
			}
		}
		mysql_free_result(res);
	}
	return this->longitude;
}

double Penalite::set_longitude(double new_longitude)
{
	if (!new_longitude) {
		std::cout << "new longitude needs to be longer" << std::endl;
		return new_longitude;
	}

	Database db;
	MYSQL_RES* res;
	if (this->codePenalite == 0) {
		mysql_free_result(db.request("UPDATE Penalite SET longitude = " + std::to_string(new_longitude) + " WHERE nom = \"" + this->nom + "\" AND duree = " + std::to_string(this->duree) + " AND latitude = " + std::to_string(this->latitude) + " AND description = \"" + this->description + '\"'));
	}
	else {
		mysql_free_result(db.request("UPDATE Penalite SET longitude = " + std::to_string(new_longitude) + " WHERE codePenalite = " + std::to_string(codePenalite)));
	}

	this->longitude = new_longitude;
	return this->longitude;
}

double Penalite::get_latitude()
{
	if (!this->latitude) {
		Database db;
		MYSQL_RES* res;
		if (this->codePenalite == 0) {
			res = db.request("SELECT latitude FROM Penalite WHERE nom = \"" + this->nom + "\" AND duree = " + std::to_string(this->duree) + " AND longitude = " + std::to_string(this->longitude) + " AND description = \"" + this->description + '\"');
		}
		else {
			res = db.request("SELECT latitude FROM Penalite WHERE codePenalite = " + std::to_string(this->codePenalite));
		}
		MYSQL_ROW row;
		if (res) {
			MYSQL_ROW row;
			while ((row = mysql_fetch_row(res)) != 0) {
				// converts ascii to int
				this->latitude = atoi(row[0]);
			}
		}
		mysql_free_result(res);
	}
	return this->latitude;
}

double Penalite::set_latitude(double new_latitude)
{
	if (!new_latitude) {
		std::cout << "new latitude needs to be longer" << std::endl;
		return new_latitude;
	}

	Database db;
	MYSQL_RES* res;
	if (this->codePenalite == 0) {
		mysql_free_result(db.request("UPDATE Penalite SET latitude = " + std::to_string(new_latitude) + " WHERE nom = \"" + this->nom + "\" AND duree = " + std::to_string(this->duree) + " AND longitude = " + std::to_string(this->longitude) + " AND description = \"" + this->description + '\"'));
	}
	else {
		mysql_free_result(db.request("UPDATE Penalite SET latitude = " + std::to_string(new_latitude) + " WHERE codePenalite = " + std::to_string(codePenalite)));
	}

	this->latitude = new_latitude;
	return this->latitude;
}


std::string Penalite::get_description() 
{
	if (this->description == "") {
		Database db;
		MYSQL_RES* res;
		if (this->codePenalite == 0) {
			res = db.request("SELECT description FROM Penalite WHERE nom = \"" + this->nom + "\" AND duree = " + std::to_string(this->duree) + " AND longitude = " + std::to_string(this->longitude) + " AND latitude = " + std::to_string(this->latitude));
		}
		else {
			res = db.request("SELECT description FROM Penalite WHERE codePenalite = " + std::to_string(this->codePenalite));
		}
		MYSQL_ROW row;
		if (res) {
			MYSQL_ROW row;
			row = mysql_fetch_row(res);
			this->description = row[0];
		}
		mysql_free_result(res);
	}
	return this->description;
}

std::string Penalite::set_description(std::string new_description) {
	if (new_description == "") {
		std::cout << "new description needs to be longer than 0" << std::endl;
		return new_description;
	}

	Database db;
	if (this->codePenalite == 0) {
		mysql_free_result(db.request("UPDATE Penalite SET description = \"" + new_description + "\" WHERE nom = \"" + this->nom + "\" AND duree = " + std::to_string(this->duree) + " AND longitude = " + std::to_string(this->longitude) + " AND latitude = " + std::to_string(this->latitude)));
	}
	else {
		mysql_free_result(db.request("UPDATE Penalite SET description = \"" + new_description + "\" WHERE codePenalite = " + std::to_string(this->codePenalite)));
	}

	this->description = new_description;
	return this->description;
}

void Penalite::delete_self() {
	Database db;
	if (!this->codePenalite) {
		mysql_free_result(db.request("DELETE FROM Penalite WHERE nom = \"" + this->nom + "\" AND duree = " + std::to_string(this->duree) + " AND longitude = " + std::to_string(this->longitude) + " AND latitude = " + std::to_string(this->latitude) + " AND description = \"" + this->description + '\"'));
	}
	else {
		mysql_free_result(db.request("DELETE FROM Penalite WHERE codePenalite = " + this->codePenalite));
	}
}