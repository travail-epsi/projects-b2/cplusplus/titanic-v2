#pragma once
#include <string>
#include <iostream>

#include <mysql.h>

class Database
{
public:
	void connectDB();
	MYSQL_RES* request(std::string);
	Database();
	~Database();

private:
	std::string host = "localhost";
	std::string user = "root";
	std::string password = "password";
	std::string databaseName = "titanic";
	int qstate;
	MYSQL* conn;
	MYSQL_ROW row;
	MYSQL_RES* res;

};
