#include "Database.h"


using namespace std;


Database::Database()
{
	connectDB();
}

Database::~Database() {};


void Database::connectDB() {
	OutputDebugString(L"Connection to database!\n");
	conn = mysql_init(0);

	conn = mysql_real_connect(conn, host.c_str(), user.c_str(), password.c_str(), databaseName.c_str(), 3306, NULL, 0);

};

MYSQL_RES* Database::request(string query) {
	if (conn) {
		cout << ("Successful connection to database!") << endl;
		qstate = mysql_query(conn, query.c_str());
		if (!qstate)
		{
			cout << ("doing request : " + query) << endl;
			// TODO: PATCH FUITE MEMOIRE : https://mariadb.com/kb/en/mariadb-connectorc-data-structures/#mysql_res
			res = mysql_store_result(conn);
			return res;
		}
		else
		{
			std::string error = "Query failed " + (std::string)mysql_error(conn) + "\n";
			cout << error << endl;
			OutputDebugStringA(error.c_str());
		}
	}
	else {
		cout << ("Failed connection to database!") << endl;
		connectDB();
	}
}


