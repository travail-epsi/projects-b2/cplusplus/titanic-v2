#pragma once
#include <string>
#include <list>
#include "../Database.h"

using namespace std;
class Membre
{
private:
	int id;
	string nom;
	string prenom;
	string sexe;
	string poste;
	int idVoilier;

public:
	Membre(string nom, string prenom, string sex, string poste, int idVoilier, int id = 0);

	static std::list<Membre> get_all();
	int get_id();
	string get_nom();
	string set_nom(string nom);
	string get_prenom();
	string set_prenom(string prenom);
	string get_sexe();
	string set_sexe(string sexe);
	string get_poste();
	string set_poste(string poste);
	int get_id_voilier();
	int set_id_voilier(int new_id);
	void delete_self();
};

