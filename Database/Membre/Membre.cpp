#include "Membre.h"

Membre::Membre(string nom, string prenom, string sexe, string poste, int idVoilier, int id) {
	if (nom != "" || prenom != "" || sexe != "" || poste != "") {
		this->id = id;
		this->nom = nom;
		this->prenom = prenom;
		this->sexe = sexe;
		this->poste = poste;
	}
	else {
		cout << "bad set of the variables for the creation of a member" << endl;
		return;
	}

	if (id == 0) {
		Database db;
		cout << "INSERT INTO Membre(nom, prenom, sexe, poste) VALUES(\'" + nom + '\'' + "," + '\'' + prenom + '\'' + ", \'" + sexe + "\' ," + '\'' + poste + "\'," + to_string(idVoilier) + ")" << endl;
		mysql_free_result(db.request("INSERT INTO Membre (nom, prenom, sexe, poste, IdVoilier) VALUES (\'" + nom + '\'' + "," + '\'' + prenom + '\'' + ", \'" + sexe + "\' ," + '\'' + poste + "\'," + to_string(idVoilier) + ")"));
		MYSQL_RES* res = db.request("SELECT LAST_INSERT_ID()");
		if (res) {
			MYSQL_ROW row;
			while ((row = mysql_fetch_row(res)) != 0) {
				// converts ascii to int
				this->id = atoi(row[0]);
			}
		}
		mysql_free_result(res);
	}
	else {
		this->id = id;
	}
}

std::list<Membre> Membre::get_all() {
	std::list<Membre> list_membre;
	Database db;
	MYSQL_RES* res = db.request("SELECT * FROM Membre");
	if (res) {
		MYSQL_ROW row;
		int i = 0;
		while ((row = mysql_fetch_row(res)) != 0) {
			
		}
	}
	return list_membre;
}

int Membre::get_id() {
	if (this->id == 0) {
		Database db;
		MYSQL_RES* res = db.request("SELECT idMembre FROM Membre WHERE nom = \"" + this->nom + '\"' + " AND prenom = \"" + this->prenom + '\"' + " AND sexe = \"" + this->sexe + '\"' + " AND poste = \"" + this->poste + '\"');
		if (res) {
			MYSQL_ROW row;
			while ((row = mysql_fetch_row(res)) != 0) {
				// converts ascii to int
				this->id = atoi(row[0]);
				cout << "the id for " + this->nom + " " + this->prenom + " is " + to_string(this->id) << endl;
			}
		}
		mysql_free_result(res);
	}
	return this->id;
}

string Membre::get_nom() {
	if (this->nom == "") {
		Database db;
		if (this->id == 0) {
			MYSQL_RES* res = db.request("SELECT nom FROM Membre WHERE prenom = \"" + this->prenom + '\"' + " AND sexe = \"" + this->sexe + '\"' + " AND poste = \"" + this->poste + '\"');
			if (res) {
				MYSQL_ROW row;
				row = mysql_fetch_row(res);
				this->nom = row[0];
			}
			mysql_free_result(res);
		}
		else {
			MYSQL_RES* res = db.request("SELECT nom FROM Membre WHERE idMembre = \"" + this->id + '\"');
			if (res) {
				MYSQL_ROW row;
				row = mysql_fetch_row(res);
				this->nom = row[0];
			}
			mysql_free_result(res);
		}
	}
	return nom;
}

string Membre::set_nom(string new_nom) {
	if (new_nom == "") {
		cout << "new name needs to be longer than 0" << endl;
		return "";
	}

	Database db;
	if (this->id == 0) {
		mysql_free_result(db.request("UPDATE Membre SET nom = \"" + new_nom + '\"' + " WHERE prenom = \"" + this->prenom + '\"' + " AND sexe = \"" + this->sexe + '\"' + " AND poste = \"" + this->poste ));
	}
	else {
		mysql_free_result(db.request("UPDATE Membre SET nom = \"" + new_nom + '\"' + " WHERE idMembre = \"" + to_string(this->id) + '\"'));
	}
	this->nom = new_nom;
	return nom;
}

string Membre::get_prenom() {
	if (this->prenom == "") {
		Database db;
		if (this->id == 0) {
			MYSQL_RES* res = db.request("SELECT prenom FROM Membre WHERE nom = \"" + this->nom + '\"' + " AND sexe = \"" + this->sexe + '\"' + " AND poste = \"" + this->poste + '\"');
			if (res) {
				MYSQL_ROW row;
				row = mysql_fetch_row(res);
				this->prenom = row[0];
			}
			mysql_free_result(res);
		}
		else {
			MYSQL_RES* res = db.request("SELECT prenom FROM Membre WHERE idMembre = \"" + to_string(this->id) + '\"');
			if (res) {
				MYSQL_ROW row;
				row = mysql_fetch_row(res);
				this->prenom = row[0];
			}
			mysql_free_result(res);
		}
	}
	return prenom;
}

string Membre::set_prenom(string new_prenom) {
	if (new_prenom == "") {
		cout << "new surname needs to be longer than 0" << endl;
		return "";
	}

	Database db;
	if (id == 0) {
		mysql_free_result(db.request("UPDATE Membre SET prenom = \"" + new_prenom + '\"' + " WHERE nom = \"" + this->nom + '\"' + " AND sexe = \"" + this->sexe + '\"' + " AND poste = \"" + this->poste));
	}
	else {
		mysql_free_result(db.request("UPDATE Membre SET nom = \"" + new_prenom + '\"' + " WHERE idMembre = \"" + to_string(this->id) + '\"'));
	}
	this->prenom = new_prenom;
	return prenom;
}

string Membre::get_sexe() {
	if (this->sexe == "") {
		Database db;
		if (this->id == 0) {
			MYSQL_RES* res = db.request("SELECT sexe FROM Membre WHERE nom = \"" + this->nom + '\"' + " AND prenom = \"" + this->prenom + '\"' + " AND poste = \"" + this->poste + '\"');
			if (res) {
				MYSQL_ROW row;
				row = mysql_fetch_row(res);
				this->sexe = row[0];
			}
			mysql_free_result(res);
		}
		else {
			MYSQL_RES* res = db.request("SELECT sexe FROM Membre WHERE idMembre = \"" + to_string(this->id) + '\"');
			if (res) {
				MYSQL_ROW row;
				row = mysql_fetch_row(res);
				this->sexe = row[0];
			}
			mysql_free_result(res);
		}
	}
	return sexe;
}

string Membre::set_sexe(string new_sexe) {
	if (new_sexe == "") {
		cout << "new sexe needs to be longer than 0" << endl;
		return "";
	}

	Database db;
	if (id == 0) {
		mysql_free_result(db.request("UPDATE Membre SET sexe = \"" + new_sexe + '\"' + " WHERE nom = \"" + this->nom + '\"' + " AND prenom = \"" + this->prenom + '\"' + " AND poste = \"" + this->poste));
	}
	else {
		mysql_free_result(db.request("UPDATE Membre SET sexe = \"" + new_sexe + '\"' + " WHERE idMembre = \"" + to_string(this->id) + '\"'));
	}
	this->sexe = new_sexe;
	return sexe;
}

string Membre::get_poste() {
	if (this->poste == "") {
		Database db;
		if (this->id != 0) {
			MYSQL_RES* res = db.request("SELECT poste FROM Membre WHERE nom = \"" + this->nom + '\"' + +" AND prenom = \"" + this->prenom + '\"' + " AND sexe = \"" + this->sexe + '\"');
			if (res) {
				MYSQL_ROW row;
				row = mysql_fetch_row(res);
				this->poste = row[0];
			}
			mysql_free_result(res);
		}
		else {
			MYSQL_RES* res = db.request("SELECT poste FROM Membre WHERE idMembre = \"" + to_string(this->id) + '\"');
			if (res) {
				MYSQL_ROW row;
				row = mysql_fetch_row(res);
				this->poste = row[0];
			}
			mysql_free_result(res);
		}
	}
	return poste;
}

string Membre::set_poste(string new_poste) {
	if (new_poste == "") {
		cout << "new poste needs to be longer than 0" << endl;
		return "";
	}

	Database db;
	if (id == 0) {
		mysql_free_result(db.request("UPDATE Membre SET poste = \"" + poste + '\"' + " WHERE nom = \"" + this->nom + '\"' + " AND prenom = \"" + this->prenom + '\"' + " AND sexe = \"" + this->sexe + '\"'));
	}
	else {
		mysql_free_result(db.request("UPDATE Membre SET poste = \"" + new_poste + '\"' + " WHERE idMembre = \"" + to_string(this->id) + '\"'));
	}
	this->poste = new_poste;
	return poste;
}

int Membre::get_id_voilier()
{
	if (!this->idVoilier) {
		Database db;
		MYSQL_RES* res;
		if (this->id == 0) {
			res = db.request("SELECT IdVoilier FROM Membre WHERE nom = \"" + this->nom + "\" AND prenom = \"" + this->prenom + "\" AND sexe = \"" + this->sexe + "\" AND poste = \"" + this->poste);
		}
		else {
			res = db.request("SELECT IdVoilier FROM Membre WHERE idMembre = " + this->id);
		}
		MYSQL_ROW row;
		if (res) {
			MYSQL_ROW row;
			while ((row = mysql_fetch_row(res)) != 0) {
				// converts ascii to int
				this->idVoilier = atoi(row[0]);
			}
		}
		mysql_free_result(res);
	}

	return this->idVoilier;
}

int Membre::set_id_voilier(int new_id) 
{
	if (!new_id) {
		std::cout << "new id needs to be longer" << std::endl;
		return new_id;
	}

	Database db;
	MYSQL_RES* res;
	if (this->idVoilier == 0) {
		mysql_free_result(db.request("UPDATE Membre SET IdVoilier = " + std::to_string(new_id) + " WHERE nom = \"" + this->nom + "\" AND prenom = \"" + this->prenom + "\" AND sexe = \"" + this->sexe + "\" AND poste = \"" + this->poste + '\"'));
	}
	else {
		mysql_free_result(db.request("UPDATE Membre SET IdVoilier = " + std::to_string(new_id) + " WHERE idMembre = " + std::to_string(this->id)));
	}

	this->idVoilier = new_id;
	return this->idVoilier;
}

void Membre::delete_self()
{
	Database db;
	if (this->id == 0) {
		mysql_free_result(db.request("DELETE FROM Membre WHERE nom = \"" + this->nom + "\" AND prenom = \"" + this->prenom + "\" AND sexe = \"" + this->sexe + "\" AND poste = \"" + this->poste + "\" AND idVoilier = " + std::to_string(this->idVoilier)));
	}
	else {
		mysql_free_result(db.request("DELETE FROM Membre WHERE idMembre = " + this->id));
	}
}