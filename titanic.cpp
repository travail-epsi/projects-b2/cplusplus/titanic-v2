// titanic.cpp : Définit le point d'entrée de l'application.
//

#include "titanic.h"
FILE* fp;

HWND hWnd;

void AddMenus(HWND);
void AddControls(HWND);
void AddMemberControls(HWND);
std::string convert_wstring_char(wchar_t*);
HMENU hMenu;


#define MEMBER_WINDOW "member_window"
#define MEMBER_WINDOW_CREATE "member_window_create"
#define MEMBER_WINDOW_GET "member_window_get"

// Variables globales :
WCHAR szTitle[MAX_LOADSTRING];                  // Texte de la barre de titre
WCHAR szWindowClass[MAX_LOADSTRING];            // nom de la classe de fenêtre principale

// Déclarations anticipées des fonctions incluses dans ce module de code :
ATOM                MyRegisterClass(HINSTANCE hInstance);
ATOM				member::RegisterMemberWindow(HINSTANCE hInstance);
ATOM				member::RegisterMemberCreateWindow(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK    WndProcMember(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK    WndProcCreateMember(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// NE PAS TOUCHER (Permet d'utiliser printf et cout)
	///////////////////////////////////////
	AllocConsole();
	freopen_s(&fp, "CONIN$", "r", stdin);
	freopen_s(&fp, "CONOUT$", "w", stdout);
	freopen_s(&fp, "CONOUT$", "w", stderr);
	///////////////////////////////////////


	// TODO: Placez le code ici.
	//Database db;
	//Penalite Pe("oscur", 3.68, 26.128, 32.687, "description");
	//std::cout << Pe.get_nom() << std::endl;
	Membre::get_all();
	




	// Initialise les chaînes globales
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_TITANIC, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);
	member::RegisterMemberWindow(hInstance);
	member::RegisterMemberCreateWindow(hInstance);
	//member::RegisterMemberGetWindow(hInstance);

	// Effectue l'initialisation de l'application :
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}



	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_TITANIC));

	MSG msg;

	// Boucle de messages principale :
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}



//
//  FONCTION : MyRegisterClass()
//
//  OBJECTIF : Inscrit la classe de fenêtre.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc; // event handler A CHANGE
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TITANIC));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_TITANIC);
	wcex.lpszClassName = szWindowClass; // nom A CHANGE
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}


//
//   FONCTION : InitInstance(HINSTANCE, int)
//
//   OBJECTIF : enregistre le handle d'instance et crée une fenêtre principale
//
//   COMMENTAIRES :
//
//        Dans cette fonction, nous enregistrons le handle de l'instance dans une variable globale, puis
//        nous créons et affichons la fenêtre principale du programme.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Stocke le handle d'instance dans la variable globale

	// Creates our window (the class name of the windows is szWindowClass)
	hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

//
//  FONCTION : WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  OBJECTIF : Traite les messages pour la fenêtre principale.
//
//  WM_COMMAND  - traite le menu de l'application
//  WM_PAINT    - Dessine la fenêtre principale
//  WM_DESTROY  - génère un message d'arrêt et retourne
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HWND memberWindow;
	switch (message)
	{
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// Analyse les sélections de menu :
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case CREATE_MEMBER_WINDOW:
		{
			// PREMIER PARAM NOM DE VOTRE FENETRE
			memberWindow = CreateWindow(TEXT(MEMBER_WINDOW), TEXT("Member window"), WS_SYSMENU | WS_VISIBLE, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, hWnd, NULL, NULL, NULL);
			break;
		}
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: Ajoutez ici le code de dessin qui utilise hdc...
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	// WM_CREATE is called when the window is created
	// adds all our menus to it
	// hWnd is the ref to our window (the window controller that holds the ref)
	case WM_CREATE:
		AddMenus(hWnd);
		AddControls(hWnd);
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}


// Gestionnaire de messages pour la boîte de dialogue À propos de.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}


void AddMenus(HWND hWnd) 
{
	// creates Menu
	hMenu = CreateMenu();
	// Create our dropdown menu
	HMENU hSubMenu = CreateMenu();
	AppendMenuW(hSubMenu, MF_STRING, NULL, L"SubMenu");

	HMENU hFileMenu = CreateMenu();
	AppendMenu(hFileMenu, MF_STRING, NULL, L"New");
	AppendMenu(hFileMenu, MF_POPUP, (UINT_PTR)hSubMenu, L"Open submenu");
	AppendMenu(hFileMenu, MF_SEPARATOR, NULL, nullptr);
	AppendMenu(hFileMenu, MF_STRING, IDM_EXIT, L"EXIT");

	// adds a menu options to our hMenu
	AppendMenu(hMenu, MF_POPUP, (UINT_PTR)hFileMenu, L"glou glou");
	AppendMenu(hMenu, MF_STRING, IDM_ABOUT, L"à propos");

	// adds menu to the given windows (passed through the window procedure handler)
	SetMenu(hWnd, hMenu);
}


void AddControls(HWND hWnd) 
{
	CreateWindowW(L"Button", L"Gestion membres",
		WS_VISIBLE | WS_CHILD | WS_BORDER | BS_MULTILINE, 0, 0, 98, 40, hWnd,
		(HMENU)CREATE_MEMBER_WINDOW, nullptr, nullptr);
}




// FIXME: la conversion de UTF16 à UTF8 détruit les caractères ASCII
std::string convert_wstring_char(wchar_t* orig) {
	std::wstring ws(orig);
	std::string text(ws.begin(), ws.end());
	return text;
}


namespace member {
	

	ATOM RegisterMemberWindow(HINSTANCE hInstance)
	{
		WNDCLASSEXW mwex;

		mwex.cbSize = sizeof(WNDCLASSEX);

		mwex.style = CS_HREDRAW | CS_VREDRAW;
		mwex.lpfnWndProc = WndProcMember;
		mwex.cbClsExtra = 0;
		mwex.cbWndExtra = 0;
		mwex.hInstance = hInstance;
		mwex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TITANIC));
		mwex.hCursor = LoadCursor(nullptr, IDC_ARROW);
		mwex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
		mwex.lpszMenuName = MAKEINTRESOURCEW(IDC_TITANIC);
		mwex.lpszClassName = TEXT(MEMBER_WINDOW);
		mwex.hIconSm = LoadIcon(mwex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

		return RegisterClassExW(&mwex);
	}

	ATOM RegisterMemberCreateWindow(HINSTANCE hInstance)
	{
		WNDCLASSEXW mwcex;

		mwcex.cbSize = sizeof(WNDCLASSEX);

		mwcex.style = CS_HREDRAW | CS_VREDRAW;
		mwcex.lpfnWndProc = WndProcCreateMember;
		mwcex.cbClsExtra = 0;
		mwcex.cbWndExtra = 0;
		mwcex.hInstance = hInstance;
		mwcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TITANIC));
		mwcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
		mwcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
		mwcex.lpszMenuName = MAKEINTRESOURCEW(IDC_TITANIC);
		mwcex.lpszClassName = TEXT(MEMBER_WINDOW_CREATE);
		mwcex.hIconSm = LoadIcon(mwcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

		return RegisterClassExW(&mwcex);
	}

	//ATOM RegisterMemberGetWindow(HINSTANCE hInstance)
	//{
	//	WNDCLASSEXW mwgex;

	//	mwgex.cbSize = sizeof(WNDCLASSEX);

	//	mwgex.style = CS_HREDRAW | CS_VREDRAW;
	//	mwgex.lpfnWndProc = ;
	//	mwgex.cbClsExtra = 0;
	//	mwgex.cbWndExtra = 0;
	//	mwgex.hInstance = hInstance;
	//	mwgex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TITANIC));
	//	mwgex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	//	mwgex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	//	mwgex.lpszMenuName = MAKEINTRESOURCEW(IDC_TITANIC);
	//	mwgex.lpszClassName = TEXT(MEMBER_WINDOW_GET);
	//	mwgex.hIconSm = LoadIcon(mwgex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	//	return RegisterClassExW(&mwgex);
	//}

	int openCreateW() {
	CreateWindow(TEXT(MEMBER_WINDOW_CREATE), TEXT("Create Member"), WS_VISIBLE | WS_SYSMENU, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, hWnd, NULL, NULL, NULL);
	return 1;
	};
}