#pragma once

#include <vector>
#include "resource.h"
#include "framework.h"
#include "mbstring.h"
#include "Database/Database.h"
#include "Database/Membre/Membre.h"
#include "Database/Penalite/Penalite.h"

#define MAX_LOADSTRING 100
#define FILE_MENU_NEW 1
#define FILE_MENU_OPEN 2
#define CREATE_MEMBER_WINDOW 3

static HINSTANCE hInst;                                // instance actuelle
static std::vector<Membre> list_membre;				   // liste des membres

INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
std::string convert_wstring_char(wchar_t* orig);

namespace member {
	ATOM RegisterMemberWindow(HINSTANCE hInstance);
	ATOM RegisterMemberCreateWindow(HINSTANCE hInstance);
	int openCreateW();
}