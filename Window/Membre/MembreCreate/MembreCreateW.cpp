#include "MembreCreateW.h"

void AddMemberCreateControls(HWND hWnd) {
	// Nom
	CreateWindowW(L"static", L"Name: ", WS_VISIBLE | WS_CHILD, 100, 50, 98, 20, hWnd, nullptr, nullptr, nullptr);
	hName = CreateWindowW(L"edit", L"", WS_VISIBLE | WS_CHILD | WS_BORDER, 200, 50, 98, 20, hWnd, nullptr, nullptr, nullptr);
	// Prenom
	CreateWindowW(L"static", L"Surname: ", WS_VISIBLE | WS_CHILD, 100, 80, 98, 20, hWnd, nullptr, nullptr, nullptr);
	hSurname = CreateWindowW(L"edit", L"", WS_VISIBLE | WS_CHILD | WS_BORDER, 200, 80, 98, 20, hWnd, nullptr, nullptr, nullptr);
	// Sexe
	CreateWindowW(L"static", L"Sexe: ", WS_VISIBLE | WS_CHILD, 100, 110, 98, 20, hWnd, nullptr, nullptr, nullptr);
	hSexe = CreateWindowW(L"edit", L"", WS_VISIBLE | WS_CHILD | WS_BORDER, 200, 110, 98, 20, hWnd, nullptr, nullptr, nullptr);
	// Poste
	CreateWindowW(L"static", L"Poste: ", WS_VISIBLE | WS_CHILD, 100, 140, 98, 20, hWnd, nullptr, nullptr, nullptr);
	hPoste = CreateWindowW(L"edit", L"", WS_VISIBLE | WS_CHILD | WS_BORDER, 200, 140, 98, 20, hWnd, nullptr, nullptr, nullptr);
	// Button
	CreateWindowW(L"Button", L"Sauvegarder\n membre",
		WS_VISIBLE | WS_CHILD | WS_BORDER | BS_MULTILINE, 150, 170, 98, 40, hWnd,
		(HMENU)CREATE_MEMBER_DB, nullptr, nullptr);
	// Text confirmation
	hOut = CreateWindowW(L"Edit", L"", WS_VISIBLE | WS_CHILD | WS_BORDER, 150, 220, 98, 40, hWnd, nullptr, nullptr, nullptr);

}


// Event handler for the Member window
LRESULT CALLBACK WndProcCreateMember(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// Analyse les s�lections de menu�:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case CREATE_MEMBER_DB:
		{
			std::string name, surname, sexe, poste;
			char out[MAX_LOADSTRING];
			wchar_t wName[MAX_LOADSTRING], wSurname[MAX_LOADSTRING], wSexe[6], wPoste[MAX_LOADSTRING];

			GetWindowText(hName, wName, MAX_LOADSTRING);
			name = convert_wstring_char(wName);
			GetWindowText(hSurname, wSurname, MAX_LOADSTRING);
			surname = convert_wstring_char(wSurname);
			GetWindowText(hSexe, wSexe, 6);
			sexe = convert_wstring_char(wSexe);
			GetWindowText(hPoste, wPoste, MAX_LOADSTRING);
			poste = convert_wstring_char(wPoste);
			std::cout << name + " " + surname + " " + sexe + " " + poste << std::endl;
			Membre new_membre(name, surname, sexe, poste, 1);
			list_membre.push_back(new_membre);


			SetWindowText(hOut, L"Membre created");
			break;
		}
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: Ajoutez ici le code de dessin qui utilise hdc...
		EndPaint(hWnd, &ps);
	}
	break;
	//case WM_DESTROY:
	//	PostQuitMessage(0);
	//	break;

		// WM_CREATE is called when the window is created
		// adds all our menus to it
		// hWnd is the ref to our window (the window controller that holds the ref)
	case WM_CREATE:
		AddMemberCreateControls(hWnd);
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}