#pragma once
#include "../../../framework.h"
#include "../../../titanic.h"
#include "../../../Database/Membre/Membre.h"

#define CREATE_MEMBER_DB 1

// Defines window Handlers
HWND hName, hSurname, hSexe, hPoste, hOut;

void AddMemberCreateControls(HWND hWnd);
LRESULT CALLBACK WndProcCreateMember(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);