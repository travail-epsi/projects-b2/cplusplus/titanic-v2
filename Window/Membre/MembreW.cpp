#include "MembreW.h"

// Manages all the controls of our application
// hWnd is the window parent
void AddMemberControls(HWND hWnd) {
	// Button
	CreateWindowW(L"Button", L"Ajouter\n membre",
		WS_VISIBLE | WS_CHILD | WS_BORDER | BS_MULTILINE, 0, 0, 98, 40, hWnd,
		(HMENU)CREATE_MEMBER, nullptr, nullptr);

	CreateWindowW(L"Button", L"R�cup�rer membre",
		WS_VISIBLE | WS_CHILD | WS_BORDER | BS_MULTILINE, 100, 0, 98, 40, hWnd,
		(HMENU)GET_MEMBER, nullptr, nullptr);
}

// Event handler for the Member window
LRESULT CALLBACK WndProcMember(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// Analyse les s�lections de menu�:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case CREATE_MEMBER:
		{
			member::openCreateW();
			cout << "coucou" << endl;
			break;
		}
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: Ajoutez ici le code de dessin qui utilise hdc...
		EndPaint(hWnd, &ps);
	}
	break;
	//case WM_DESTROY:
	//	PostQuitMessage(0);
	//	break;

		// WM_CREATE is called when the window is created
		// adds all our menus to it
		// hWnd is the ref to our window (the window controller that holds the ref)
	case WM_CREATE:
		AddMemberControls(hWnd);
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}